# Camunda external task bearer token interceptor

### Как установить библиотеку в проект
`npm i git+https://gitlab.com/yoldar/camunda-external-task-bearer-token-interceptor.git`


### Пример использования
```javascript
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');

let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}

const config = { ......... interceptors: bearerTokenInterceptor, ......... };
```


### Переменные для настройки
- **KEYCLOAK_BASE_URL**: Keycloak - Базовый URL (http://wf.test)
- **KEYCLOAK_REALM_ID**: Keycloak - Параметр из панели администратора Keycloak (bi-group)
- **KEYCLOAK_CLIENT_ID**: Keycloak - Параметр из панели администратора Keycloak (camunda-external-task-client)
- **KEYCLOAK_CLIENT_SECRET**: Keycloak - Параметр из панели администратора Keycloak (218ee2be-d615-42ab-83eb-a8286405255e)
