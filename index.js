const request = require('request-promise-native');
const MISSING_AUTH_PARAMS =
    "Couldn't instantiate BearerTokenInterceptor, missing configuration parameters"

class BearerTokenInterceptor {
  /**
   * @throws Error
   */
  constructor(options) {
    if (!options || !options.clientId || !options.clientSecret || !options.baseUrl || !options.readlmId) {
      throw new Error(MISSING_AUTH_PARAMS);
    }
    /**
     * Bind member methods
     */
    this.requestToken = this.requestToken.bind(this);
    this.interceptor = this.interceptor.bind(this);
    this.options = options;

    this.requestToken();
    return this.interceptor;
  }

  async requestToken() {
    try{
      const response = await request({
        url: `${this.options.baseUrl}/auth/realms/${this.options.readlmId}/protocol/openid-connect/token`,
        method: 'POST',
        auth: {
          user: this.options.clientId,
          pass: this.options.clientSecret
        },
        form: {
          'grant_type': 'client_credentials',
        }
      });
      const json = JSON.parse(response);
      // console.log('Access token updated');
      this.header = { Authorization: `Bearer ${json.access_token}` };
      this.timeout = (json.expires_in * 1000) - (1000 * 60 * 1); // update token before 1 minute to expire
      setTimeout(this.requestToken, this.timeout);
    } catch(error) {
      console.error(error);
      setTimeout(this.requestToken, 5000);
    }
  }   

  interceptor(config) {
    return { ...config, headers: { ...config.headers, ...this.header } };
  }
}

module.exports = BearerTokenInterceptor;
